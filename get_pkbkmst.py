import numpy as np
import iiiraven2 as ir2


def get_data(root, run, dtype, include_mult, multiples, condition_mst, usefull=False):
    path = '/Users/krishna/Research/PhD/Neutrinos/MST/2018_toomanyhaloes/'
    if dtype == 'pofk':
        data = np.load(path + 'pkbkmst/halo_' + root + '_' + str(run) + '_pofk.npz')
        d = data['pk']
    elif dtype == 'bofk':
        data = np.load(path + 'pkbkmst/halo_' + root + '_' + str(run) + '_bofk.npz')
        d = abs(data['bk'])
    elif dtype == 'mst':
        if include_mult is True:
            for n in range(1, multiples+1):
                if n == 1:
                    data = np.load(path + 'pkbkmst/halo_' + root + '_' + str(run) + '_mst.npz')
                    _d = np.concatenate([data['dh'], data['lh'], data['bh'], data['sh']])
                    d = _d
                else:
                    data = np.load(path + 'pkbkmst/halo_' + root + '_' + str(run) + '_mst_part_' + str(n) + '.npz')
                    _d = np.concatenate([data['dh'], data['lh'], data['bh'], data['sh']])
                    d = d + _d
            d = d / float(multiples)
        else:
            usemean = True
            if usemean is True:
                data = np.load(path + 'pkbkmst/halo_' + root + '_' + str(run) + 'mst_mean.npz')
                d = data['d']
            else:
                data = np.load(path + 'pkbkmst/halo_' + root + '_' + str(run) + '_mst.npz')
                d = np.concatenate([data['dh'], data['lh'], data['bh'], data['sh']])
                d = d
        if usefull is False:
            d = d[condition_mst]
    return d


def get_data_combos(root, run, which_method, include_mult, multiples, condition_mst, usefull=False):
    if which_method == 'pk':
        return get_data(root, run, 'pofk', include_mult, multiples, condition_mst, usefull=usefull)
    elif which_method == 'bk':
        return get_data(root, run, 'bofk', include_mult, multiples, condition_mst, usefull=usefull)
    elif which_method == 'mst':
        return get_data(root, run, 'mst', include_mult, multiples, condition_mst, usefull=usefull)
    elif which_method == 'pk_bk':
        return np.concatenate([get_data(root, run, 'pofk', include_mult, multiples, condition_mst, usefull=usefull),
                               get_data(root, run, 'bofk', include_mult, multiples, condition_mst, usefull=usefull)])
    elif which_method == 'pk_mst':
        return np.concatenate([get_data(root, run, 'pofk', include_mult, multiples, condition_mst, usefull=usefull),
                               get_data(root, run, 'mst', include_mult, multiples, condition_mst, usefull=usefull)])
    elif which_method == 'bk_mst':
        return np.concatenate([get_data(root, run, 'bofk', include_mult, multiples, condition_mst, usefull=usefull),
                               get_data(root, run, 'mst', include_mult, multiples, condition_mst, usefull=usefull)])
    elif which_method == 'pk_bk_mst':
        return np.concatenate([get_data(root, run, 'pofk', include_mult, multiples, condition_mst, usefull=usefull),
                               get_data(root, run, 'bofk', include_mult, multiples, condition_mst, usefull=usefull),
                               get_data(root, run, 'mst', include_mult, multiples, condition_mst,  usefull=usefull)])
    else:
        print which_method + ' is not supported.'


def get_fiducial(which_method, condition_mst, usefull=False, include_mult=False, multiples=100):
    root = 'fiducial'
    n = 1000
    fid_data = []
    for i in range(1, n+1):
        run = i
        data = get_data_combos(root, run, which_method, include_mult, multiples, condition_mst, usefull=usefull)
        fid_data.append(data)
        ir2.progress_bar(i-1, n, explanation='Getting Fiducial')
    fid_data = np.array(fid_data)
    return fid_data


def get_mock(which_method, condition_mst, usefull=False, include_mult=False, multiples=100):
    root = 'mock'
    n = 10
    mock_data = []
    for i in range(1, n+1):
        run = i
        data = get_data_combos(root, run, which_method, include_mult, multiples, condition_mst, usefull=usefull)
        mock_data.append(data)
        ir2.progress_bar(i-1, n, explanation='Getting Mock')
    mock_data = np.array(mock_data)
    return mock_data


def get_grid(which_method, condition_mst, usefull=False, include_mult=False, multiples=100):

    # Load Grid1, Grid2, Grid3, Grid4, Grid5 and Grid6 Info

    path = '/Users/krishna/Research/PhD/Neutrinos/MST/2018_toomanyhaloes/'

    data = np.load(path + 'param_files/grid1_counts.npz')
    as_grid1 = data['as_grid']*1e9
    om_grid1 = data['om_grid']
    mv_grid1 = data['mv_grid']
    g1_count1 = data['count1']
    g1_count2 = data['count2']
    g1_count3 = data['count3']

    data = np.load(path + 'param_files/grid2_counts.npz')
    as_grid2 = data['as_grid']*1e9
    om_grid2 = data['om_grid']
    mv_grid2 = data['mv_grid']
    g2_count1 = data['count1']
    g2_count2 = data['count2']
    g2_count3 = data['count3']

    data = np.load(path + 'param_files/grid3_counts.npz')
    as_grid3 = data['as_grid']*1e9
    om_grid3 = data['om_grid']
    mv_grid3 = data['mv_grid']
    g3_count1 = data['count1']
    g3_count2 = data['count2']
    g3_count3 = data['count3']

    data = np.load(path + 'param_files/grid4_counts.npz')
    as_grid4 = data['as_grid']*1e9
    om_grid4 = data['om_grid']
    mv_grid4 = data['mv_grid']
    g4_count1 = data['count1']
    g4_count2 = data['count2']
    g4_count3 = data['count3']

    data = np.load(path + 'param_files/grid5_counts.npz')
    as_grid5 = data['as_grid']*1e9
    om_grid5 = data['om_grid']
    mv_grid5 = data['mv_grid']
    g5_count1 = data['count1']
    g5_count2 = data['count2']
    g5_count3 = data['count3']

    data = np.load(path + 'param_files/grid6_counts.npz')
    as_grid6 = data['as_grid']*1e9
    om_grid6 = data['om_grid']
    mv_grid6 = data['mv_grid']
    g6_count1 = data['count1']
    g6_count2 = data['count2']
    g6_count3 = data['count3']

    data = np.load(path + 'param_files/grid7_counts.npz')
    as_grid7 = data['as_grid']*1e9
    om_grid7 = data['om_grid']
    mv_grid7 = data['mv_grid']
    g7_count1 = data['count1']
    g7_count2 = data['count2']
    g7_count3 = data['count3']

    num_haloes = 100000.
    num_par = num_haloes + 10000
    m = 3

    mean_data = []

    # Grid1 sims
    for i in range(0, len(as_grid1)):
        run = i+1
        if g1_count1[i] > num_par and g1_count2[i] > num_par and g1_count3[i] > num_par:
            for j in range(1, m+1):
                root = 'grid1' + str(j)
                if j == 1:
                    d = get_data_combos(root, run, which_method, include_mult, multiples, condition_mst, usefull=usefull)
                else:
                    _d = get_data_combos(root, run, which_method, include_mult, multiples, condition_mst, usefull=usefull)
                    d += _d
            d =  d/float(m)
            mean_data.append(d)
        ir2.progress_bar(i, len(as_grid1), explanation='Getting Grid1')

    # Grid2 sims
    for i in range(0, len(as_grid2)):
        run = i+1
        if g2_count1[i] > num_par and g2_count2[i] > num_par and g2_count3[i] > num_par:
            for j in range(1, m+1):
                root = 'grid2' + str(j)
                if j == 1:
                    d = get_data_combos(root, run, which_method, include_mult, multiples, condition_mst, usefull=usefull)
                else:
                    _d = get_data_combos(root, run, which_method, include_mult, multiples, condition_mst, usefull=usefull)
                    d += _d
            d =  d/float(m)
            mean_data.append(d)
        ir2.progress_bar(i, len(as_grid2), explanation='Getting Grid2')

    # Grid3 sims
    for i in range(0, len(as_grid3)):
        run = i+1
        if g3_count1[i] > num_par and g3_count2[i] > num_par and g3_count3[i] > num_par:
            for j in range(1, m+1):
                root = 'grid3' + str(j)
                if j == 1:
                    d = get_data_combos(root, run, which_method, include_mult, multiples, condition_mst, usefull=usefull)
                else:
                    _d = get_data_combos(root, run, which_method, include_mult, multiples, condition_mst, usefull=usefull)
                    d += _d
            d =  d/float(m)
            mean_data.append(d)
        ir2.progress_bar(i, len(as_grid3), explanation='Getting Grid3')

    # Grid4 sims
    for i in range(0, len(as_grid4)):
        run = i+1
        if g4_count1[i] > num_par and g4_count2[i] > num_par and g4_count3[i] > num_par:
            for j in range(1, m+1):
                root = 'grid4' + str(j)
                if j == 1:
                    d = get_data_combos(root, run, which_method, include_mult, multiples, condition_mst, usefull=usefull)
                else:
                    _d = get_data_combos(root, run, which_method, include_mult, multiples, condition_mst, usefull=usefull)
                    d += _d
            d =  d/float(m)
            mean_data.append(d)
        ir2.progress_bar(i, len(as_grid4), explanation='Getting Grid4')

    # Grid5 sims
    for i in range(0, len(as_grid5)):
        run = i+1
        if g5_count1[i] > num_par and g5_count2[i] > num_par and g5_count3[i] > num_par:
            for j in range(1, m+1):
                root = 'grid5' + str(j)
                if j == 1:
                    d = get_data_combos(root, run, which_method, include_mult, multiples, condition_mst, usefull=usefull)
                else:
                    _d = get_data_combos(root, run, which_method, include_mult, multiples, condition_mst, usefull=usefull)
                    d += _d
            d =  d/float(m)
            mean_data.append(d)
        ir2.progress_bar(i, len(as_grid5), explanation='Getting Grid5')

    # Grid6 sims
    for i in range(0, len(as_grid6)):
        run = i+1
        if g6_count1[i] > num_par and g6_count2[i] > num_par and g6_count3[i] > num_par:
            for j in range(1, m+1):
                root = 'grid6' + str(j)
                if j == 1:
                    d = get_data_combos(root, run, which_method, include_mult, multiples, condition_mst, usefull=usefull)
                else:
                    _d = get_data_combos(root, run, which_method, include_mult, multiples, condition_mst, usefull=usefull)
                    d += _d
            d =  d/float(m)
            mean_data.append(d)
        ir2.progress_bar(i, len(as_grid6), explanation='Getting Grid6')

    # Grid7 sims
    for i in range(0, len(as_grid7)):
        run = i+1
        if g7_count1[i] > num_par and g7_count2[i] > num_par and g7_count3[i] > num_par:
            for j in range(1, m+1):
                root = 'grid7' + str(j)
                if j == 1:
                    d = get_data_combos(root, run, which_method, include_mult, multiples, condition_mst, usefull=usefull)
                else:
                    _d = get_data_combos(root, run, which_method, include_mult, multiples, condition_mst, usefull=usefull)
                    d += _d
            d =  d/float(m)
            mean_data.append(d)
        ir2.progress_bar(i, len(as_grid7), explanation='Getting Grid7')

    mean_data = np.array(mean_data)

    return mean_data


def get_grid_params():
    data = np.load('param_info.npz')
    return data['as_grid'], data['om_grid'], data['mv_grid'], data['s8_grid']


def get_mock_params():
    data = np.load('param_info.npz')
    return data['as_mock'], data['om_mock'], data['mv_mock'], data['s8_mock']


def get_fiducial_params():
    data = np.load('param_info.npz')
    return data['as_fid'], data['om_fid'], data['mv_fid'], data['s8_fid']
