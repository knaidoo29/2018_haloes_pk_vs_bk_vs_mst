import numpy as np
from scipy.special import loggamma


def get_chi2(data, model, precision):
    chi2 = ((data - model).T).dot(precision.dot(data - model))
    return chi2


def get_lnC(N, M, covariance):
    N, M = float(N), float(M)
    p = float(len(covariance))
    sign, logdet = np.linalg.slogdet(covariance)
    print 'ln(determinant) = ', sign*logdet
    logdet = sign*logdet
    lnC = loggamma(N/2.) - loggamma((N-p)/2.) + 0.5*np.log(M/(M+1)) - 0.5*logdet - (p/2.)*np.log(np.pi*(N-1.))
    return lnC


def get_lnP(data, model, precision, N, M):
    N, M = float(N), float(M)
    return -(N/2.)*np.log(1 + ((M/((M+1.)*(N-1.)))*get_chi2(data, model, precision)))



def get_nearest(as_, om_, mv_, as_min, as_max, om_min, om_max, mv_min, mv_max, as_grid, om_grid, mv_grid, r_threshold):
    as_n = as_ - as_min
    om_n = om_ - om_min
    mv_n = mv_ - mv_min
    as_n /= as_max - as_min
    om_n /= om_max - om_min
    mv_n /= mv_max - mv_min
    as_gn = as_grid - as_min
    om_gn = om_grid - om_min
    mv_gn = mv_grid - mv_min
    as_gn /= as_max - as_min
    om_gn /= om_max - om_min
    mv_gn /= mv_max - mv_min
    r = np.sqrt((as_gn - as_n)**2. + (om_gn - om_n)**2. + (mv_gn - mv_n)**2.)
    r_min = r.min()
    if r_min <= r_threshold:
        return 0.
    else:
        return -np.inf